CLI {Command Line Interface} Commands

ls - {list} the files and folders contained by the current directory

pwd - {print working directory/present working directory} - shows the current folder we are working on.

cd <folderName/path to folder> - {changes the directory} - change the current folder/directory we are currently working on our CLI (terminal/gitbash)

mkdir <folderName> - create a new directory

Sublime Text 4
>>lightweight text/file editor
>>use less RAM/Memory which is important because we also, as devs have our google chrome open ...

Configure our Git

git config --global user.email "joshuazarate09@gmail.com"
--this allows us to identify the account from gitlab or github who will push/upload files into our online gitlab or github services...

git config --global user.name "jmzarate09"
--this will also allow us to determine the name of the person who uploades thee latest commit/version in our repo...

Pushing for the very first time

git init
--initialize a local folder as local git repo.
--means that the folder and its files are now tracked by git

git add .
--allows us to add all the files and updated into a new commit/version of our files and folders to be uploaded to our online repo.

git commit -m <commitMessages>
--allows us to create a new version of our files and folders based on the updates added using git add.

git remote add origin <gitUrlOnlineRepo>
--this allows us to connect an online repo to a local repo
--"origin" is the default or conventional name for an online repo

git push origin master
--allows us to push/upload the latest commit created into our online repo that us desiganted as "origin"
--master is the default version or the main version of our files in the repo


git add .
git commit -m "<commitMessages>"
git push origin master 